package ru.aushakov.tm.constant;

public class TerminalConst {

    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_HELP = "help";

}
