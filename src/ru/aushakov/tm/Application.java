package ru.aushakov.tm;

import ru.aushakov.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parseArgs(args);
    }

    private static void parseArgs(String[] args) {
        if (args == null || args.length < 1) return;
        switch (args[0]) {
            case TerminalConst.CMD_ABOUT:
                showAppInfo();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            default:
                System.out.println("The argument entered is not supported!");
        }
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void showAppInfo() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ANDREY USHAKOV");
        System.out.println("E-MAIL: oroktavy@gmail.com");
        System.exit(0);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.CMD_ABOUT + " - Show general application info");
        System.out.println(TerminalConst.CMD_VERSION + " - Show application version");
        System.out.println(TerminalConst.CMD_HELP + " - Show possible arguments");
        System.exit(0);
    }

}
