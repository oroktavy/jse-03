# TASK MANAGER

# DEVELOPER INFO

Name: Andrey Ushakov

E-mail: oroktavy@gmail.com

# SOFTWARE

* JDK 1.8
* Windows 10

# HARDWARE

* RAM 16 GB
* CPU Intel i5
* HDD 128 GB

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://drive.google.com/drive/folders/1WjW5fFlsEPzo-G42igc3sVqtHesikH6D?usp=sharing